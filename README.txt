Aplikacja symuluj�ca stanowisko pracownicze w fabryce. Dane do symulacji obci��e� i awarii s� zbierane na podstawie czujnik�w dost�pnych
w smartfonach. Aplikacja symuluje r�wnie� awarie systemu i wymaga od u�ytkownika �agodzeniu ich. Po zbyt du�ym czasie braku reakcji 
u�ytkownika, program uruchamia alarm, kt�ra mo�na unicestwi� poprzez potrz��ni�cie urz�dzeniem. Program wykorzystuje system logowania
zarz�dzany za pomoc� SQLite.

Aplikacja z rozszerzeniem .apk (gotowym do instalacji na urz�dzeniu z systemem Android) znajduje si� w podkatalogu: Apk