package com.plightlab.milszym.plightlab;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import leonids.ParticleSystem;

public class HistoryActivity extends AppCompatActivity {

    private String username;
    private TextView historyTextView;
    private Button skipButton;
    private Thread historyThread;
    private int counter;
    private List<String> messages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        counter = 0;
        username = getIntent().getStringExtra("user");

        final ParticleSystem ps = new ParticleSystem(this, 100, R.drawable.star_pink, 800);
        final ParticleSystem ps2 = new ParticleSystem(this, 100, R.drawable.star_white, 800);

        messages = new ArrayList<String>();
        messages.add(getResources().getString(R.string.historyMessage1));
        messages.add(getResources().getString(R.string.historyMessage2));
        messages.add(getResources().getString(R.string.historyMessage3));
        messages.add(getResources().getString(R.string.historyMessage4));
        messages.add(getResources().getString(R.string.historyMessage5));
        messages.add(getResources().getString(R.string.historyMessage6));
        messages.add(getResources().getString(R.string.historyMessage7));

        Typeface typeFace = Typeface.createFromAsset(getAssets(), "ComicalCartoon.ttf");
        historyTextView = (TextView) findViewById(R.id.historyTextView);
        historyTextView.setTypeface(typeFace);
        historyTextView.setText(getResources().getString(R.string.historyWelcome) + username + "!");

        skipButton = (Button) findViewById(R.id.skipButton);

        skipButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                ps.setScaleRange(0.7f, 1.3f);
                ps.setSpeedRange(0.1f, 0.25f);
                ps.setRotationSpeedRange(90, 180);
                ps.setFadeOut(200, new AccelerateInterpolator());
                ps.oneShot(v, 70);

                ps2.setScaleRange(0.7f, 1.3f);
                ps2.setSpeedRange(0.1f, 0.25f);
                ps.setRotationSpeedRange(90, 180);
                ps2.setFadeOut(200, new AccelerateInterpolator());
                ps2.oneShot(v, 70);

                startWorkspaceActivity();
            }
        });

        historyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateTextViewOut();
            }
        });

    }


    public void goToNextMessage() {
        historyTextView.setText(messages.get(counter));
        counter++;
    }

    public void animateTextViewOut() {
        Animation fadingOut = AnimationUtils.loadAnimation(this, R.anim.get_out_from_here);
        fadingOut.setFillAfter(true);
        historyTextView.startAnimation(fadingOut);

        fadingOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation arg0) {
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                animateTextViewIn();
            }
        });
    }

    public void animateTextViewIn() {
        if (counter < 7) {
            goToNextMessage();
            Animation fadingIn = AnimationUtils.loadAnimation(this, R.anim.get_in_there);
            fadingIn.setFillAfter(true);
            historyTextView.startAnimation(fadingIn);

        } else
            startWorkspaceActivity();
    }


    public void startWorkspaceActivity() {
        Intent i = new Intent(this, WorkspaceActivity.class);
        i.putExtra("user", username);
        startActivity(i);
    }
}
