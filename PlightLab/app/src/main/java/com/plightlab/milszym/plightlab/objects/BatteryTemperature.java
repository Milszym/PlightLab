package com.plightlab.milszym.plightlab.objects;

/**
 * Created by Szczypiorek on 19.04.2017.
 */

public class BatteryTemperature {

    private static final double MAX_TEMPERATURE=65.0;
    private static final double MIN_TEMPERATURE=-20.0;
    private static final double OPTIMAL_MAX_TEMPERATURE=31.0;
    private static final double OPTIMAL_MIN_TEMPERATURE=28.0;

    public BatteryTemperature(){
    }

    public double optimalTemperature(){

        double randTemperature = Math.random()*(OPTIMAL_MAX_TEMPERATURE-OPTIMAL_MIN_TEMPERATURE)+OPTIMAL_MIN_TEMPERATURE;

        return randTemperature;
    }

}

