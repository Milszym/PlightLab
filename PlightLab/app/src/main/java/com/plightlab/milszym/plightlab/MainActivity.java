package com.plightlab.milszym.plightlab;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import leonids.ParticleSystem;

public class MainActivity extends AppCompatActivity {

    private TextView welcomeMessage;
    private ImageButton goOnButton;
    private ViewGroup milszymsLayout;
    private Thread startOtherActivityThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        welcomeMessage = (TextView) findViewById(R.id.welcomeMessageTextView);
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "ComicalCartoon.ttf");
        welcomeMessage.setTypeface(typeFace);

        goOnButton = (ImageButton) findViewById(R.id.goOnButton);
        milszymsLayout = (ViewGroup) findViewById(R.id.milszymsLayout);

        milszymsLayout.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        anotherAnimation(welcomeMessage);
                        return true;
                    }
                }
        );

        startOtherActivityThread = new Thread(new RunnableWaitForAnotherActivity(this));
    }


    public void anotherAnimation(View view) {
        //TextView view = (TextView)findViewById(R.id.welcomeMessageTextView);
        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(this, R.anim.hyperspace_jump);
//        hyperspaceJumpAnimation.setStartTime(AnimationUtils.currentAnimationTimeMillis()+2000);
//        view.setAnimation(hyperspaceJumpAnimation);
        hyperspaceJumpAnimation.setFillAfter(true);
        view.startAnimation(hyperspaceJumpAnimation);
        goOnButton.setVisibility(View.VISIBLE);
        Animation buttonGoOnAnimation = AnimationUtils.loadAnimation(this, R.anim.button_go_on_animation);
        goOnButton.setAnimation(buttonGoOnAnimation);
        buttonGoOnAnimation.setFillAfter(true);

    }

    public void buttonGoOnClicked(View view) {
        ParticleSystem ps = new ParticleSystem(this, 100, R.drawable.star_pink, 800);
        ps.setScaleRange(0.7f, 1.3f);
        ps.setSpeedRange(0.1f, 0.25f);
        ps.setRotationSpeedRange(90, 180);
        ps.setFadeOut(200, new AccelerateInterpolator());
        ps.oneShot(view, 70);

        ParticleSystem ps2 = new ParticleSystem(this, 100, R.drawable.star_white, 800);
        ps2.setScaleRange(0.7f, 1.3f);
        ps2.setSpeedRange(0.1f, 0.25f);
        ps.setRotationSpeedRange(90, 180);
        ps2.setFadeOut(200, new AccelerateInterpolator());
        ps2.oneShot(view, 70);
        startOtherActivityThread.start();
    }

    public class RunnableWaitForAnotherActivity implements Runnable {

        private Context context;

        public RunnableWaitForAnotherActivity(Context context) {
            this.context = context;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            startActivity(new Intent(context, LoginActivity.class));
        }
    }

}
