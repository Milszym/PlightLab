package com.plightlab.milszym.plightlab;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.plightlab.milszym.plightlab.com.plightlab.milszym.database.DBAdapter;

import leonids.ParticleSystem;

public class LoginActivity extends AppCompatActivity {

    private TextView loginMessage, registerMessage, howToRegisterInfo;
    private TextInputLayout tilName, tilPassword, tilCheckPassword;
    private Button loginButton, createButton;
    private boolean isLogin;
    private DBAdapter usersDatabase;
    private Thread startOtherActivityThread;
    private Thread animationRegisterInfoThread;

    public enum ActionStatus {LOGIN, CREATE}

    private ActionStatus actionStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        howToRegisterInfo = (TextView)findViewById(R.id.howToRegisterInfo);
        animateFadeInOut();

        loginMessage = (TextView) findViewById(R.id.loginMessage);
        registerMessage = (TextView) findViewById(R.id.registerMessage);
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "ComicalCartoon.ttf");

        loginMessage.setTypeface(typeFace);
        registerMessage.setTypeface(typeFace);

        tilName = (TextInputLayout) findViewById(R.id.textInputLayoutName);
        tilPassword = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);
        tilCheckPassword = (TextInputLayout) findViewById(R.id.textInputLayoutCheckPassword);

        loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setTypeface(typeFace);

        createButton = (Button) findViewById(R.id.createButton);
        createButton.setTypeface(typeFace);


        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;
        shaken = false;
        sensorSensitiveness = 12;

        isLogin = true;

        openDB();
    }


    public void onShakeCreateAccount(ActionStatus action) {
        //deklaracja animacji
        Animation getOut = AnimationUtils.loadAnimation(this, R.anim.get_out_from_here);
        Animation getOut2 = AnimationUtils.loadAnimation(this, R.anim.get_out_from_here);
        Animation getIn = AnimationUtils.loadAnimation(this, R.anim.get_in_there);
        Animation getIn2 = AnimationUtils.loadAnimation(this, R.anim.get_in_there);
        getOut.setFillAfter(true);
        getOut2.setFillAfter(true);
        getIn.setFillAfter(true);
        getIn2.setFillAfter(true);

        //jezeli chcemy logowac sie lub utworzyc konto-  to animacje wyjscia i wejscia bede podlaczone do odpowiednich obiektów typu View
        switch (action) {
            case LOGIN: {
                howToRegisterInfo.setText("Potrząśnij by się zarejestrować");
                loginButton.startAnimation(getIn);
                tilCheckPassword.startAnimation(getOut);
                createButton.startAnimation(getOut);
                loginMessage.startAnimation(getIn);
                registerMessage.startAnimation(getOut2);
                break;
            }
            case CREATE: {
                howToRegisterInfo.setText("Potrząśnij by się zalogowac");
                loginButton.startAnimation(getOut);
                loginMessage.startAnimation(getOut);
                tilCheckPassword.startAnimation(getIn);
                createButton.startAnimation(getIn);
                registerMessage.startAnimation(getIn2);
                registerMessage.setVisibility(View.VISIBLE);
                tilCheckPassword.setVisibility(View.VISIBLE);
                createButton.setVisibility(View.VISIBLE);
                break;
            }
        }

    }


    //inicjacja czujnika zyroskopowego - sprawdzanie czy ktos potrzasnal urzadzeniem
    private SensorManager mSensorManager;
    private float mAccel; // acceleration apart from gravity
    private float mAccelCurrent; // current acceleration including gravity
    private float mAccelLast; // last acceleration including gravity
    private boolean shaken;
    private long currTime;
    private int sensorSensitiveness;
    private final SensorEventListener mSensorListener = new SensorEventListener() {

        public void onSensorChanged(SensorEvent se) {
            float x = se.values[0];
            float y = se.values[1];
            float z = se.values[2];
            mAccelLast = mAccelCurrent;
            mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
            float delta = mAccelCurrent - mAccelLast;
            mAccel = mAccel * 0.9f + delta; // perform low-cut filter
            if (mAccel > sensorSensitiveness) {
                if (shaken == false) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Device has shaken.", Toast.LENGTH_LONG);
                    toast.show();
                    shaken = true;
                    if (isLogin == true) {
                        onShakeCreateAccount(actionStatus.CREATE);
                        isLogin = false;
                    } else if (isLogin == false) {
                        onShakeCreateAccount(actionStatus.LOGIN);
                        isLogin = true;
                    }
                    currTime = System.currentTimeMillis();
                }
                if (currTime + 1000 < System.currentTimeMillis()) {
                    shaken = false;
                }
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(mSensorListener);
        super.onPause();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        closeDB();
    }

    private void closeDB(){
        usersDatabase.close();
    }

    public void openDB() {
        usersDatabase = new DBAdapter(this);
        usersDatabase.open();
    }

    public void onClickRegisterUser(View view) {

        ParticleSystem ps = new ParticleSystem(this, 100, R.drawable.confeti3, 800);
        ps.setScaleRange(0.7f, 1.3f);
        ps.setSpeedRange(0.1f, 0.25f);
        ps.setAcceleration(0.0001f, 90);
        ps.setRotationSpeedRange(90, 180);
        ps.setFadeOut(200, new AccelerateInterpolator());
        ps.oneShot(view, 100);

        tilPassword.getEditText().setTextColor(Color.WHITE);
        tilCheckPassword.getEditText().setTextColor(Color.WHITE);
        tilName.getEditText().setTextColor(Color.WHITE);


        //sprawdzamy czy przypadkiem nie jest puste to wszystko co wpisujemy i dodajemy uzytkownika
        if (!TextUtils.isEmpty(tilName.getEditText().getText().toString())
                && !TextUtils.isEmpty(tilPassword.getEditText().getText().toString())
                && !TextUtils.isEmpty(tilCheckPassword.getEditText().getText().toString())) {
                //sprawdzamy czy hasla sie ze soba zgadzaja (potwierdzenie + zwykle haslo)
                if(tilPassword.getEditText().getText().toString().equals(tilCheckPassword.getEditText().getText().toString())){

                    String name=tilName.getEditText().getText().toString();
                    String pass = tilPassword.getEditText().getText().toString();
                    //sprawdzamy czy taki uzytkownik juz istnieje
                    Cursor cursor = usersDatabase.getRowByUsername(name);
                    if(!cursor.moveToFirst()){
                        usersDatabase.insertRow(name,pass);
                        System.out.println("Insertowanko");
                        onShakeCreateAccount(actionStatus.LOGIN);
                        isLogin = true;
                    }
                    else{
                        tilName.getEditText().setTextColor(Color.RED);
                        tilName.getEditText().setText("Taki login juz istnieje");
                    }

                }
                else{
                    tilPassword.getEditText().setText("Hasła nie pasuja do siebie");
                    tilPassword.getEditText().setTextColor(Color.RED);
                    tilCheckPassword.getEditText().setText("Hasla nie pasuja do siebie");
                    tilCheckPassword.getEditText().setTextColor(Color.RED);
                }

        }
    }

    //funkcja sprawdzajaca czy istnieje taki uzytkownik w bazie danych
    public void onClickLogin(View view){

        ParticleSystem ps = new ParticleSystem(this, 100, R.drawable.star_white_border, 800);
        ps.setScaleRange(0.7f, 1.3f);
        ps.setSpeedRange(0.1f, 0.25f);
        ps.setAcceleration(0.0001f, 90);
        ps.setRotationSpeedRange(90, 180);
        ps.setFadeOut(200, new AccelerateInterpolator());
        ps.oneShot(view, 100);

        tilPassword.getEditText().setTextColor(Color.WHITE);
        tilCheckPassword.getEditText().setTextColor(Color.WHITE);
        tilName.getEditText().setTextColor(Color.WHITE);

        String tmpUsername = tilName.getEditText().getText().toString();
        String tmpPassword = tilPassword.getEditText().getText().toString();
        Cursor cursor = usersDatabase.getRowByUsernameAndPassword(tmpUsername, tmpPassword);
        if(cursor.moveToFirst()){
            startOtherActivityThread = new Thread(new RunnableWaitForAnotherActivity(this, tmpUsername));
            startOtherActivityThread.start();
        }
        else{
            tilName.getEditText().setTextColor(Color.RED);
            tilName.getEditText().setText("Dane sie nie zgadzaja");
            tilPassword.getEditText().setTextColor(Color.RED);
            tilPassword.getEditText().setText("Dane sie nie zgadzaja");
        }

    }

    //opoznienie w przejsciu do nowego activity, po to by wszystkie animacje mogły się wykonać
    public class RunnableWaitForAnotherActivity implements Runnable {

        private Context context;
        private String username;

        public RunnableWaitForAnotherActivity(Context context, String username) {
            this.context = context;
            this.username = username;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Intent i = new Intent(context, HistoryActivity.class);
            i.putExtra("user", username);
            startActivity(i);
        }
    }

    //animacja napisu informujacego co zrobic gdy sie nie ma konta (napis rejestracji)
    public void animateFadeInOut(){
        Animation fading = new AlphaAnimation(0, 1);
        fading.setInterpolator(new DecelerateInterpolator()); //add this
        fading.setDuration(2000);
        fading.setRepeatMode(Animation.RESTART);
        fading.setFillAfter(true);
        fading.setRepeatMode(Animation.REVERSE);
        fading.setRepeatCount(Animation.INFINITE);

        howToRegisterInfo.setAnimation(fading);
    }





}
