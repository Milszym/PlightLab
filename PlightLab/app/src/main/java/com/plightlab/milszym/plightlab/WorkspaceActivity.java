package com.plightlab.milszym.plightlab;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.Image;
import android.media.MediaPlayer;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.plightlab.milszym.plightlab.R;
import com.plightlab.milszym.plightlab.objects.Alarm;
import com.plightlab.milszym.plightlab.objects.BatteryTemperature;
import com.plightlab.milszym.plightlab.objects.BreakdownController;
import com.plightlab.milszym.plightlab.opengl.MyGLSurfaceView;

import org.w3c.dom.Text;

import java.util.List;

import leonids.ParticleSystem;

public class WorkspaceActivity extends AppCompatActivity {

    private SensorManager mSensorManagerTemperature, mSensorManagerLight;
    private Sensor mSensorTemperature, mSensorLight;
    private ActivityManager activityManager;
    private ActivityManager.MemoryInfo mi;
    private pl.droidsonroids.gif.GifTextView gifPositive, gifNegative;

    private enum PointerRotation {ZERO_TO_LEFT, LEFT_TO_RIGHT, RIGHT_TO_LEFT}

    ;
    private boolean pointerRotationFlag;

    //zmienne od systemu temperatury
    private ParticleSystem ps;
    private ImageView temperatureSystem, pointerImage;
    private TextView temperatureTextView, lightTextView, memoryUsageTextView, whosLabIsItTextView;
    private boolean userTemperatureReactionFlag;
    private BatteryTemperature bt;
    private String currentColor, futureColor;
    private int currColor;
    private ImageView frozenScreen, redBackground;
    private BreakdownController temperatureBreakdownSystem;

    //zmienne od systemu wolnej pamieci
    private ImageView wrenchImage;
    private long availableMemory, totalMemory;
    private Thread memoryUsageThread, batteryTemperatureThread;
    private boolean userMemoryUsageReactionFlag;
    private BreakdownController memoryUsageBreakdownSystem;

    //zmienne dotyczace oswietlenia
    private ImageView bulbImageView, bulbLightImageView;
    private boolean userLightReactionFlag;
    private float actualLightIntensiveness, futureLightIntensiveness;
    private BreakdownController lightBreakdownSystem;




    private Camera camera;
    private Camera.Parameters cameraParameters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workspace);

        //deklaracja Viewsów

        //ImageViews
        temperatureSystem = (ImageView) findViewById(R.id.temperatureSystem);
        currentColor = "#00FF0D";
        temperatureSystem.setBackgroundColor(Color.parseColor(currentColor));
        //currColor = Color.parseColor(currentColor);
        pointerImage = (ImageView) findViewById(R.id.pointerImage);
        frozenScreen = (ImageView) findViewById(R.id.frozenView);
        wrenchImage = (ImageView) findViewById(R.id.wrenchImageView);
        bulbLightImageView = (ImageView) findViewById(R.id.bulbLightImageView);
        bulbImageView = (ImageView) findViewById(R.id.bulbImageView);
        redBackground = (ImageView)findViewById(R.id.redAlertBackground);

        //TextVIews
        temperatureTextView = (TextView) findViewById(R.id.temperatureTextView);
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "ComicalCartoon.ttf");
        temperatureTextView.setTypeface(typeFace);

        lightTextView = (TextView) findViewById(R.id.lightTextView);
        lightTextView.setTypeface(typeFace);

        memoryUsageTextView = (TextView) findViewById(R.id.memoryUsageTextView);
        memoryUsageTextView.setTypeface(typeFace);

        gifPositive = (pl.droidsonroids.gif.GifTextView) findViewById(R.id.gifPositive);
        gifNegative = (pl.droidsonroids.gif.GifTextView) findViewById(R.id.gifNegative);
        gifNegative.setVisibility(View.GONE);

        whosLabIsItTextView = (TextView)findViewById(R.id.whosLabTextView);
        whosLabIsItTextView.setTypeface(typeFace);
        whosLabIsItTextView.setText(getResources().getString(R.string.whosLab)+"\n"+getIntent().getStringExtra("user"));

        //deklaracja czujnika temperatury systemowej
        mSensorManagerTemperature = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorTemperature = mSensorManagerTemperature.getDefaultSensor(Sensor.TYPE_TEMPERATURE);
        SensorManager sensor = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //lightBreakdownSystem = new BreakdownController(40,20, this, sensor);


        //deklaracja czujnika swiatla
        mSensorManagerLight = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorLight = mSensorManagerLight.getDefaultSensor(Sensor.TYPE_LIGHT);
        actualLightIntensiveness = 0.1f;
        futureLightIntensiveness=0.2f;
        lightBreakdownSystem = new BreakdownController(40,20, this, sensor, redBackground);


        //deklaracja czujnika zmian zużycia pamięci
        mi = new ActivityManager.MemoryInfo();
        activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        totalMemory = mi.totalMem;
        memoryUsageThread = new Thread(new MemoryUsage());
        memoryUsageThread.start();
        memoryUsageBreakdownSystem = new BreakdownController(40, 15,this, sensor, redBackground);

        //dodatkowe deklaracje czujnika temperatury
        bt = new BatteryTemperature();
        batteryTemperatureThread = new Thread(new BatteryTemperatureChanges());
        batteryTemperatureThread.start();
        temperatureBreakdownSystem = new BreakdownController(40,25, this, sensor, redBackground);

        animatePointer(PointerRotation.ZERO_TO_LEFT);
        pointerRotationFlag = false;
        //animateTemperatureColor();


        //zapobieganie awarii temperatury za pomoca długiego przycisniecia na termomemtr
        userTemperatureReactionFlag = false;
        temperatureSystem.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                userTemperatureReactionFlag = true;
                return true;
            }
        });

        //zapobieganie awarii zuzycia pamieci za pomoca długiego przycisniecia na tasme produkcyjna
        userMemoryUsageReactionFlag = false;
        gifPositive.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                userMemoryUsageReactionFlag = true;
                return true;
            }
        });

        gifNegative.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                userMemoryUsageReactionFlag = true;
                return true;
            }
        });

        //zapobieganie awarii swiatla za pomoca długiego przycisniecia na żarówke
        userLightReactionFlag=false;
        bulbImageView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                userLightReactionFlag = true;
                return true;
            }
        });

    }

    //funkcja animujaca wskaznik na termometrze
    public void animatePointer(PointerRotation pointerRotation) {
        switch (pointerRotation) {

            case ZERO_TO_LEFT: {
                Animation rotation = AnimationUtils.loadAnimation(this, R.anim.pointer_rotator_left);
                rotation.setFillAfter(true);
                pointerImage.startAnimation(rotation);
                break;
            }
            case RIGHT_TO_LEFT: {
                Animation rotation = AnimationUtils.loadAnimation(this, R.anim.pointer_rotator_from_right_to_left);
                rotation.setFillAfter(true);
                pointerImage.startAnimation(rotation);
                break;
            }
            case LEFT_TO_RIGHT: {
                Animation rotation = AnimationUtils.loadAnimation(this, R.anim.pointer_rotator_right);
                rotation.setFillAfter(true);
                pointerImage.startAnimation(rotation);
                break;
            }
        }
    }

    //funkcja animujaca temperature - kolory sa zamieniane na wartosci HSV, tak aby przejscie z zielonej temperatury na czerwona przechodzilo przez kolor zolty
    public void animateTemperatureColorHsv(double power) {
        double blue, green, red;
        int blueInt, greenInt, redInt;
        red = green = blue = 0.0;

        if (0 <= power && power < 0.5) {
            green = 1.0;
            red = 2 * power;
            if (pointerRotationFlag == false) {
                animatePointer(PointerRotation.RIGHT_TO_LEFT);
                pointerRotationFlag = true;
            }
        }
        if (0.5 <= power && power <= 1) {
            red = 1.0;
            green = 1.0 - 2 * (power - 0.5);
            if (pointerRotationFlag == true) {
                animatePointer(PointerRotation.LEFT_TO_RIGHT);
                pointerRotationFlag = false;
            }
        }
        final float[] from = new float[3],
                to = new float[3];

        red = red * 255;
        green = green * 255;
        blue = blue * 255;
        blueInt = (int) blue;
        redInt = (int) red;
        greenInt = (int) green;


//
//        Color.colorToHSV(Color.parseColor(currentColor), from); // from white
//        Color.colorToHSV(Color.parseColor(futureColor), to);     // to red


        Color.colorToHSV(currColor, from); // from white
        Color.colorToHSV(Color.rgb(redInt, greenInt, blueInt), to);     // to red

        currColor = Color.rgb(redInt, greenInt, blueInt);

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);   // animate from 0 to 1
        anim.setDuration(1000);                              // for 300 ms

        final float[] hsv = new float[3];                  // transition color
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Transition along each axis of HSV (hue, saturation, value)
                hsv[0] = from[0] + (to[0] - from[0]) * animation.getAnimatedFraction();
                hsv[1] = from[1] + (to[1] - from[1]) * animation.getAnimatedFraction();
                hsv[2] = from[2] + (to[2] - from[2]) * animation.getAnimatedFraction();

                temperatureSystem.setBackgroundColor(Color.HSVToColor(hsv));
            }
        });

        anim.start();
    }

    //czujnik temperatury wykrywajacy jej zmiane
    private final SensorEventListener mSensorListenerTemperature = new SensorEventListener() {

        public void onSensorChanged(SensorEvent se) {
            float temperature = se.values[0];
            temperatureTextView.setText(temperature + "  " + (char) 176 + "C");
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    //czujnik swiatla wykrywajacy zmiane oswietlenia
    private final SensorEventListener mSensorListenerLight = new SensorEventListener() {

        public void onSensorChanged(SensorEvent se) {
            double luxes = Double.valueOf(se.values[0]);
            lightTextView.setText(String.format("%.2f", luxes) + " luksow");
            if (luxes >= 3000) {
                luxes = 1.0;
            }
            else {
                luxes = luxes / 3000;
            }
            futureLightIntensiveness = (float)luxes;
            lightHandler.sendEmptyMessage(0);
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };


    //deklaracja zachowania sensorów w przypadku gdy Activity wejdzie w stan pauzy oraz wróci ze stanu pauzy do stanu działania
    @Override
    protected void onResume() {
        super.onResume();
        mSensorManagerLight.registerListener(mSensorListenerLight, mSensorLight, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManagerTemperature.registerListener(mSensorListenerTemperature, mSensorTemperature, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        mSensorManagerLight.unregisterListener(mSensorListenerLight);
        mSensorManagerTemperature.unregisterListener(mSensorListenerTemperature);
        super.onPause();
    }

    //klasy potrzebne do uruchomienia wątków (sprawdzaja stan poszczegolnych sensorów co jakis czas)
    public class MemoryUsage implements Runnable {


        public MemoryUsage() {
        }

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                availableMemoryTextViewHandler.sendEmptyMessage(0);
            }
        }
    }

    public class BatteryTemperatureChanges implements Runnable {

        BatteryTemperatureChanges() {
        }

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep((Math.round(Math.random() * 1000)));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                temperatureTextViewHandler.sendEmptyMessage(0);
            }
        }
    }

    //handlery wykonujace zadania powierzone watkom

    Handler temperatureTextViewHandler = new Handler() {
        private int temperature;
        private double power;

        @Override
        public void handleMessage(Message msg) {
            if (!temperatureBreakdownSystem.checkTheBreakdown()) {
                temperature = Integer.valueOf(String.valueOf(Math.round(bt.optimalTemperature())));
                //60 to bardzo wysoka temperatura
                power = Double.valueOf(String.valueOf((temperature - 20))) / 61;
                temperatureTextView.setText(String.valueOf(temperature) + " " + (char) 176 + "C");
                animateTemperatureColorHsv(power);
            } else {
                temperature = Integer.valueOf(String.valueOf(Math.round(Math.random() * (60 - 55) + 55)));
                power = Double.valueOf(String.valueOf((temperature))) / 61;
                temperatureTextView.setText(String.valueOf(temperature) + " " + (char) 176 + "C");
                animateTemperatureColorHsv(power);
            }
            if(userTemperatureReactionFlag){
                animateFrozenScreen();
                userTemperatureReactionFlag=false;
                temperatureBreakdownSystem.setUserReaction(true);
            }
        }

    };

    Handler availableMemoryTextViewHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (!memoryUsageBreakdownSystem.checkTheBreakdown()) {
                activityManager.getMemoryInfo(mi);
                availableMemory = mi.availMem / 1048576L;
                memoryUsageTextView.setText(String.valueOf(availableMemory) + " z " + String.valueOf(mi.totalMem / 1048576L) + " MB");
                gifPositive.setVisibility(View.VISIBLE);
                gifNegative.setVisibility(View.GONE);
            } else {
                int rand = (int) Math.random() * 50 + 150;
                memoryUsageTextView.setText(String.valueOf(mi.totalMem / 1048576L - rand) + " z " + String.valueOf(mi.totalMem / 1048576L) + " MB");
                gifPositive.setVisibility(View.GONE);
                gifNegative.setVisibility(View.VISIBLE);
            }
            if(userMemoryUsageReactionFlag){
                animateWrench();
                userMemoryUsageReactionFlag=false;
                memoryUsageBreakdownSystem.setUserReaction(true);
            }
        }

    };

    Handler lightHandler = new Handler(){

        private Animation fading;
        private static final float MAX_INTENSIVENESS = 1.0f;

        @Override
        public void handleMessage(Message msg) {
            if(!lightBreakdownSystem.checkTheBreakdown()) {
                fading = new AlphaAnimation(actualLightIntensiveness, futureLightIntensiveness);
                fading.setInterpolator(new DecelerateInterpolator()); //add this
                fading.setDuration(500);
                fading.setFillAfter(true);
                actualLightIntensiveness = futureLightIntensiveness;
                bulbLightImageView.startAnimation(fading);
            }
            else{
                flashLight(false);
                double luxes = Math.random()*10000+5000;
                lightTextView.setText(String.format("%.2f", luxes) + " luksow");
                futureLightIntensiveness = MAX_INTENSIVENESS;
                fading = new AlphaAnimation(actualLightIntensiveness, futureLightIntensiveness);
                fading.setInterpolator(new DecelerateInterpolator()); //add this
                fading.setDuration(500);
                fading.setFillAfter(true);
                actualLightIntensiveness = futureLightIntensiveness;
                bulbLightImageView.startAnimation(fading);
            }
            if(userLightReactionFlag){
                userLightReactionFlag=false;
                redBackground.setVisibility(View.GONE);
                lightBreakdownSystem.setUserReaction(true);
            }
//            fadeTheLight(actualLightIntensiveness, futureLightIntensiveness);
        }


    };

    //animowanie zamrozonego termometru jako naprawienia problemu z temperatura
    public void animateFrozenScreen() {
        frozenScreen.setVisibility(View.VISIBLE);
        Animation fading = new AlphaAnimation(0, 1);
        fading.setInterpolator(new DecelerateInterpolator()); //add this
        fading.setDuration(500);
        fading.setRepeatMode(Animation.REVERSE);
        fading.setRepeatCount(1);
        fading.setFillAfter(true);

        frozenScreen.setAnimation(fading);
    }

    //animacja klucza jako naprawienie problemu ze zuzyciem pamieci
    public void animateWrench() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.wrench_animation);
        anim.setFillAfter(true);
        wrenchImage.setVisibility(View.VISIBLE);
        wrenchImage.setAnimation(anim);

    }

    //uruchomienie latarki z tylu urzadzenia jako sygnalizacja ze cos jest nie tak ze swiatlem
    public void flashLight(boolean turnOff) {
        boolean hasFlash = getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        if (!hasFlash) {

        }
        else{
            try {
                releaseCameraAndPreview();
                cameraParameters = camera.getParameters();
                cameraParameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                camera.setParameters(cameraParameters);
                camera.startPreview();
            } catch (Exception e) {
                Log.e(getString(R.string.app_name), "failed to open Camera");
                e.printStackTrace();
            }
        }

    }

    //funkcja sprawdzajaca czy kamera nie jest przypadkiem w użyciu (jezeli jest, zwolni jej uzycie, by mogla byc uzywana przez nasza aplikacje)
    private void releaseCameraAndPreview() {

        if (camera != null) {
            camera.release();
            camera = null;
        }
    }


}
