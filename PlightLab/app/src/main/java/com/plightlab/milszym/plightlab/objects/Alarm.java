package com.plightlab.milszym.plightlab.objects;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.plightlab.milszym.plightlab.R;

/**
 * Created by Szczypiorek on 24.04.2017.
 */

public class Alarm {

    private static boolean isAnimation=false;
    private static boolean isEndOfAnimation=false;

    private MediaPlayer mediaPlayer;
    private Context context;
    private ImageView redBackground;

    public Alarm(Context context, ImageView redBackground){
        this.redBackground = redBackground;
        this.context = context;
        mediaPlayer = new MediaPlayer();
        mediaPlayer = MediaPlayer.create(context, R.raw.lelo_lelo);
        mediaPlayer.setLooping(true);
    }

    public void launchTheAlarm(){
        if(!isAnimation) {
            isAnimation=true;
            redBackground.setVisibility(View.VISIBLE);
            animation();
        }
        mediaPlayer.start();
    }

    public void shutdownTheAlarm(){
        redBackground.setVisibility(View.GONE);
        try {
            stopAnimation();
        } catch (Exception e) {
            e.printStackTrace();
        }
        isAnimation=false;
        redBackground.setVisibility(View.GONE);
        mediaPlayer.stop();
        //mediaPlayer.release();
    }

    //zapetla animacje
    public void animation(){
        Animation fading = new AlphaAnimation(0, 0.8f);
        fading.setInterpolator(new DecelerateInterpolator()); //add this
        fading.setDuration(1000);
        fading.setRepeatMode(Animation.RESTART);
        fading.setFillAfter(true);
        fading.setRepeatMode(Animation.REVERSE);
        fading.setRepeatCount(Animation.INFINITE);

        redBackground.setAnimation(fading);
    }

    //ustawia animacje na schodzenie do przezroczystosci
    public void stopAnimation(){
        if(!isEndOfAnimation) {
            isEndOfAnimation=true;
            redBackground.getAnimation().cancel();
            Animation fading2 = new AlphaAnimation(0.5f, 0);
            fading2.setInterpolator(new DecelerateInterpolator()); //add this
            fading2.setDuration(100);
            fading2.setFillAfter(true);
            redBackground.setAnimation(fading2);
        }
    }

}
