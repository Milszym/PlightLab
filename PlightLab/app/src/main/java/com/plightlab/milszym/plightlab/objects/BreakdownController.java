package com.plightlab.milszym.plightlab.objects;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Szczypiorek on 24.04.2017.
 */

public class BreakdownController {

    private Alarm alarm;

    private int breakdownStartCounter;
    private int breakdownDurabilityCounter;
    private int breakdown;
    private boolean hasBreakdownRandom;

    private boolean userReaction;

    private Context context;

    private int durability, frequency;

    private static final int MAX_DURABILITY = 30;
    private static final int FREQUENCY = 50;

    private boolean userAlarmPrevention;

    private ImageView redBackground;

    public BreakdownController(int durability, int frequency, Context context, SensorManager mSensorManager, ImageView redBackground) {
        this.redBackground = redBackground;
        alarm = new Alarm(context, redBackground);

        userAlarmPrevention = false;

        hasBreakdownRandom=false;
        breakdownDurabilityCounter=0;
        breakdownStartCounter=0;
        userReaction = false;
        this.durability = durability;
        this.frequency = frequency;
        this.context = context;

        this.mSensorManager = mSensorManager;
        //mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;
        shaken = false;
        sensorSensitiveness = 12;

    }

    public boolean checkTheBreakdown() {

        if(userAlarmPrevention){
            hasBreakdownRandom=false;
            userAlarmPrevention=false;
            breakdownStartCounter=0;
            breakdownDurabilityCounter=0;
        }

        if (hasBreakdownRandom == false) {
            breakdown = Integer.valueOf(String.valueOf(Math.round(Math.random() * FREQUENCY)));
            hasBreakdownRandom = true;
            return false;
        } else if (breakdownStartCounter == breakdown && breakdownDurabilityCounter != MAX_DURABILITY) { //jezeli licznik osiagnal breakdown to zaczynamy go
            breakdownDurabilityCounter += 1;
            if (userReaction == true) {
                breakdown = Integer.valueOf(String.valueOf(Math.round(Math.random() * FREQUENCY)));
                breakdownStartCounter = 0;
                breakdownDurabilityCounter = 0;
                userReaction = false;
                hasBreakdownRandom = false;
                return false;
            }
            return true;
        } else if (breakdownStartCounter == breakdown && breakdownDurabilityCounter == MAX_DURABILITY) { //koniec breakdownu (po 5 wejsciach do funkcji)
            alarm.launchTheAlarm();
            return true;
        } else {
            breakdownStartCounter += 1;
            return false;
        }
    }



    public void handleUserReaction() {
        userReaction = true;
    }

    public void resetUserReaction(){
        userReaction=false;
    }

    public boolean isUserReaction() {
        return userReaction;
    }

    public void setUserReaction(boolean userReaction) {
        this.userReaction = userReaction;
    }

    private SensorManager mSensorManager;
    private float mAccel; // acceleration apart from gravity
    private float mAccelCurrent; // current acceleration including gravity
    private float mAccelLast; // last acceleration including gravity
    private boolean shaken;
    private long currTime;
    private int sensorSensitiveness;

    private final SensorEventListener mSensorListener = new SensorEventListener() {


        public void onSensorChanged(SensorEvent se) {
            float x = se.values[0];
            float y = se.values[1];
            float z = se.values[2];
            mAccelLast = mAccelCurrent;
            mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
            float delta = mAccelCurrent - mAccelLast;
            mAccel = mAccel * 0.9f + delta; // perform low-cut filter
            if (mAccel > sensorSensitiveness) {
                if (shaken == false) {
                    Toast toast = Toast.makeText(context, "Alarm wyłączony.", Toast.LENGTH_LONG);
                    toast.show();
                    userAlarmPrevention=true;
                    alarm.shutdownTheAlarm();
                    alarm = new Alarm(context, redBackground);
                    shaken = true;
                    currTime = System.currentTimeMillis();
                }
                if (currTime + 1000 < System.currentTimeMillis()) {
                    shaken = false;
                }
            }



        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

    };

}
